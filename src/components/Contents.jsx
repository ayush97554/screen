import React, { Component } from 'react';
import { Switch,Checkbox,Input } from 'antd';
import { Menu, Dropdown, Button, message, Tooltip } from 'antd';
import { DownOutlined,UserOutlined,UpOutlined} from '@ant-design/icons';
import { useState } from 'react';
import { Table, Radio, Divider } from 'antd';
import Demo from './ComponentTable'
class Contents extends Component {
    state = {  }
    handleMenuClick=(e)=> {
        message.info('Click on menu item.');
        console.log('click', e);
      }
      
    render() { 
      
        
        const menu = (
            <Menu onClick={this.handleMenuClick}>
              <Menu.Item key="1">
                <UserOutlined />
                1st menu item
              </Menu.Item>
              <Menu.Item key="2">
                <UserOutlined />
                2nd menu item
              </Menu.Item>
              <Menu.Item key="3">
                <UserOutlined />
                3rd item
              </Menu.Item>
            </Menu>
          );
        return ( 
            <React.Fragment>
                <div style={{padding:"1vw"}}>
            <div style={{display:"flex",justifyContent:"space-between",marginBottom:"1vw"}}>
            <h1>Manage Regulatory Requirements</h1>
            <div><Button type="primary">CREATE DPT</Button>&nbsp;
            <span>Tag Components&nbsp;</span>
            <Switch /></div>
            </div>
            <Dropdown overlay={menu} >
      <Button style={{border:"2px solid black",width:"100%",display:"flex",justifyContent:"space-between",marginBottom:"1vw"}}>
        Component Name <DownOutlined style={{paddingTop:"0.5vw"}} />
      </Button>
    </Dropdown>
    <div style={{display:"flex"}}>

    <div style={{width:"80%",border:"1px",borderRightStyle:"dotted" ,margin:"1vw"}}>
      <div style={{display:"flex",justifyContent:"space-around"}}>
      <span style={{fontWeight:"bold"}}>
        <Checkbox />&nbsp; List of components
        </span>
        <Input placeholder="Search a component" style={{width:"40%",border:"2px solid black"}}/>
        <span style={{fontWeight:"bold"}}>Save Preferences <br />
        ADD
        </span>
      </div>
      <hr style={{border:"1.5px",borderTopStyle:"dotted"}}/>
      <table>
          <tr >
              
              <td>
                  <span>Component01&emsp;</span>
               </td>   
                <td ><span style={{marginLeft:"11vw"}}>Detail01&emsp;</span>  </td>
                <td>  Detail01&emsp;</td>
                <td>  Detail01&emsp;</td>
                <td>  Detail01&emsp;</td>
            
                
          </tr>
          <tr >
              <td>
                  <span>Component01&emsp;</span>
               </td>   
                <td ><span style={{marginLeft:"11vw"}}>Detail01&emsp;</span>  </td>
                <td>  Detail01&emsp;</td>
                <td>  Detail01&emsp;</td>
                <td>  Detail01&emsp;</td>
          </tr>
          <tr >
              <td>
                  <span>Component01&emsp;</span>
               </td>   
                <td ><span style={{marginLeft:"11vw"}}>Detail01&emsp;</span>  </td>
                <td>  Detail01&emsp;</td>
                <td>  Detail01&emsp;</td>
                <td>  Detail01&emsp;</td>
          </tr>
          <tr >
              <td>
                  <span>Component01&emsp;</span>
               </td>   
                <td ><span style={{marginLeft:"11vw"}}>Detail01&emsp;</span>  </td>
                <td>  Detail01&emsp;</td>
                <td>  Detail01&emsp;</td>
                <td>  Detail01&emsp;</td>
          </tr>
          <tr >
              <td>
                  <span>Component01&emsp;</span>
               </td>   
                <td ><span style={{marginLeft:"11vw"}}>Detail01&emsp;</span>  </td>
                <td>  Detail01&emsp;</td>
                <td>  Detail01&emsp;</td>
                <td>  Detail01&emsp;</td>
          </tr>
          <tr >
              <td>
                  <span>Component01&emsp;</span>
               </td>   
                <td ><span style={{marginLeft:"11vw"}}>Detail01&emsp;</span>  </td>
                <td>  Detail01&emsp;</td>
                <td>  Detail01&emsp;</td>
                <td>  Detail01&emsp;</td>
          </tr>
          <tr >
              <td>
                  <span>Component01&emsp;</span>
               </td>   
                <td ><span style={{marginLeft:"11vw"}}>Detail01&emsp;</span>  </td>
                <td>  Detail01&emsp;</td>
                <td>  Detail01&emsp;</td>
                <td>  Detail01&emsp;</td>
          </tr>
          <tr >
              <td>
                  <span>Component01&emsp;</span>
               </td>   
                <td ><span style={{marginLeft:"11vw"}}>Detail01&emsp;</span>  </td>
                <td>  Detail01&emsp;</td>
                <td>  Detail01&emsp;</td>
                <td>  Detail01&emsp;</td>
          </tr>
          <tr >
              <td>
                  <span>Component01&emsp;</span>
               </td>   
                <td ><span style={{marginLeft:"11vw"}}>Detail01&emsp;</span>  </td>
                <td>  Detail01&emsp;</td>
                <td>  Detail01&emsp;</td>
                <td>  Detail01&emsp;</td>
          </tr>
          <tr >
              <td>
                  <span>Component01&emsp;</span>
               </td>   
                <td ><span style={{marginLeft:"11vw"}}>Detail01&emsp;</span>  </td>
                <td>  Detail01&emsp;</td>
                <td>  Detail01&emsp;</td>
                <td>  Detail01&emsp;</td>
          </tr>
          <tr >
              <td>
                  <span>Component01&emsp;</span>
               </td>   
                <td ><span style={{marginLeft:"11vw"}}>Detail01&emsp;</span>  </td>
                <td>  Detail01&emsp;</td>
                <td>  Detail01&emsp;</td>
                <td>  Detail01&emsp;</td>
          </tr>
      </table>
      
      {/* new table code from here---------------------------------- */}
    

    </div> 
    
    <div >
  
  <div style={{display:"flex",justifyContent:"space-between",fontWeight:"bold"}}>
      <span >
      Filter by&emsp;
  </span>
  <span> (08)</span>
  </div>
  <Dropdown overlay={menu} >
      <Button style={{border:"1px solid black",display:"flex",justifyContent:"space-between",marginBottom:"1vw",fontWeight:"bold"}}>
        Countries (185) <UpOutlined style={{paddingTop:"0.5vw"}} />
      </Button>
    </Dropdown>
    <Button style={{border:"1px solid black",display:"flex",justifyContent:"space-between",marginBottom:"1vw",fontWeight:"bold",paddingRight:"2.9vw"}}>
        Search country 
      </Button>
      <Checkbox /><Checkbox />&emsp;Country 01<br />
      <Checkbox /><Checkbox />&emsp;Country 02<br />
      <Checkbox /><Checkbox />&emsp;Country 03<br />
      <Checkbox /><Checkbox />&emsp;Country 04<br />
      <Checkbox /><Checkbox />&emsp;Country 05<br />
      <Checkbox /><Checkbox />&emsp;Country 06<br />
      <Checkbox /><Checkbox />&emsp;Country 07<br />
    </div>
    </div>
        
        
        <hr style={{border:"1.5px",borderTopStyle:"dotted"}}/>
        </div>
   </React.Fragment>
        );
    }
}
 
export default Contents;