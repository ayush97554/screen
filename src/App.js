import React from 'react';
import { Layout, Divider } from 'antd';

import './App.css';
import 'antd/dist/antd.css';
import Headercontent from './components/Headercontent'
import Sidercontent from './components/Sidercontent'
import Footercontent from './components/Footercontent'
import Contents from './components/Contents'

const { Header, Footer, Sider, Content } = Layout;

function App() {
  return (<div style={{border:"2px solid black",width:"80%",margin:"auto"}}> <Layout>
    <Sider style={{background:"white",border:"1px",borderRightStyle:" dotted"}}><Sidercontent /></Sider>
    <Layout style={{background:"white"}}>
     <Headercontent  />
      <Content style={{background:"white"}}><Contents/></Content>
     <Footercontent />
    </Layout>
  </Layout></div>);
}

export default App;
